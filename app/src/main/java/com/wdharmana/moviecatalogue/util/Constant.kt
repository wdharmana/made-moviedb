package com.wdharmana.moviecatalogue.util

class Constant {

    companion object {
        const val TAG = "MovieCatalogue"
        const val POSTER_URL = "https://image.tmdb.org/t/p"
    }

}