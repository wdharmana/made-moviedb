package com.wdharmana.moviecatalogue.util

import java.util.*

class AppUtils {

    companion object {

        fun getPosterUrl(path: String): String {
            return "${Constant.POSTER_URL}/w185/"+path
        }

        fun getLangCode(locale: Locale): String {
            return if(locale.language == "in")  {
                "id-ID"
            }  else {
                "en-US"
            }
        }

    }

}