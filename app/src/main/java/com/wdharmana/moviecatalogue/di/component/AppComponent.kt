package com.wdharmana.moviecatalogue.di.component

import com.wdharmana.moviecatalogue.MovieApp
import com.wdharmana.moviecatalogue.di.module.ActivityModule
import com.wdharmana.moviecatalogue.di.module.AppModule
import com.wdharmana.moviecatalogue.di.module.NetworkModule
import com.wdharmana.moviecatalogue.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    ActivityModule::class,
    ViewModelModule::class,
    NetworkModule::class])
interface AppComponent {

    fun inject(instance: MovieApp)

}