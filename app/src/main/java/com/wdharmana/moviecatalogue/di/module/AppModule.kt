package com.wdharmana.moviecatalogue.di.module

import android.app.Application
import androidx.room.Room
import com.wdharmana.moviecatalogue.local.FavoriteDao
import com.wdharmana.moviecatalogue.local.MovieLocalDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideMovieLocalDB(): MovieLocalDB = Room.databaseBuilder(app,
            MovieLocalDB::class.java, "movies_db").build()

    @Provides
    @Singleton
    fun provideFavoriteDao(db: MovieLocalDB): FavoriteDao = db.favoriteDao()

}