package com.wdharmana.moviecatalogue.di.module

import com.wdharmana.moviecatalogue.view.home.ItemFragment
import com.wdharmana.moviecatalogue.view.home.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeItemFragment(): ItemFragment

}