package com.wdharmana.moviecatalogue.view.home

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.wdharmana.moviecatalogue.model.Movie
import com.wdharmana.moviecatalogue.service.DataManager
import com.wdharmana.moviecatalogue.util.Constant
import javax.inject.Inject

class MovieViewModel @Inject
constructor(private val dataManager: DataManager) : ViewModel() {

    val movieListLiveData: MutableLiveData<List<Movie>> = MutableLiveData()

    val isShowLoader: MutableLiveData<Boolean> = MutableLiveData()
    val isInsertFavoriteSuccess: MutableLiveData<Boolean> = MutableLiveData()
    val errorMsg: MutableLiveData<String> = MutableLiveData()


    @SuppressLint("CheckResult")
    fun getMovies() {
        isShowLoader.value = true
        dataManager.getMovies()
                .subscribe({ result ->
                    Log.e(Constant.TAG, Gson().toJson(result))
                    isShowLoader.value = false
                    if (result.isSuccessful) {
                        val res = result.body()

                        res?.results?.let {
                            movieListLiveData.value = it
                        }
                    }

                },
                        { error ->
                            isShowLoader.value = false
                            errorMsg.value = error.message.toString()
                            Log.e(Constant.TAG, error.message)

                        }
                )

    }

    @SuppressLint("CheckResult")
    fun getTvShow() {
        isShowLoader.value = true
        dataManager.getTvShow()
                .subscribe({ result ->
                    Log.e(Constant.TAG, Gson().toJson(result))
                    isShowLoader.value = false
                    if (result.isSuccessful) {
                        val res = result.body()

                        res?.results?.let {
                            movieListLiveData.value = it
                        }
                    }

                },
                        { error ->
                            isShowLoader.value = false
                            errorMsg.value = error.message.toString()
                            Log.e(Constant.TAG, error.message)
                        }
                )

    }


    @SuppressLint("CheckResult")
    fun getFavorites(cat: String) {
        isShowLoader.value = true
        dataManager.getFavorites(cat)
                .subscribe({ result ->

                    Log.e(Constant.TAG, Gson().toJson(result))

                    isShowLoader.value = false

                    val movieList = arrayListOf<Movie>()
                    for (fav in result) {

                        val movie = Movie(
                                id = fav.id,
                                overview = fav.overview,
                                voteAverage = fav.voteAverage,
                                posterPath = fav.posterPath,
                                backdropPath = fav.backdropPath
                        )

                        if (cat == "movie") {
                            movie.apply {
                                title = fav.title
                                releaseDate = fav.releaseDate
                            }
                        } else {
                            movie.apply {
                                name = fav.title
                                firstAirDate = fav.releaseDate
                            }
                        }

                        movieList.add(movie)

                    }
                    movieListLiveData.value = movieList

                },
                        { error ->
                            isShowLoader.value = false
                            errorMsg.value = error.message.toString()
                            Log.e(Constant.TAG, error.message)
                        }
                )

    }

    /*
    @SuppressLint("CheckResult")
    fun insertFavorite(favorite: Favorite) {
        isShowLoader.value = true
        dataManager.insertFavorite(favorite)
                .subscribe({ result ->

                    isShowLoader.value = false
                    isInsertFavoriteSuccess.value = true

                },
                        { error ->
                            isShowLoader.value = false
                            errorMsg.value = error.message.toString()
                            Log.e(Constant.TAG, error.message)
                        }
                )

    }

    @SuppressLint("CheckResult")
    fun deleteFavorite(id: String) {
        isShowLoader.value = true
        dataManager.deleteFavorite(id)
                .subscribe({ result ->

                    isShowLoader.value = false
                    isInsertFavoriteSuccess.value = true

                },
                        { error ->
                            isShowLoader.value = false
                            errorMsg.value = error.message.toString()
                            Log.e(Constant.TAG, error.message)
                        }
                )

    }
    */

}