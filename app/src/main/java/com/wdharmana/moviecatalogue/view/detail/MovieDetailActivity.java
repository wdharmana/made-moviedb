package com.wdharmana.moviecatalogue.view.detail;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wdharmana.moviecatalogue.R;
import com.wdharmana.moviecatalogue.model.Movie;
import com.wdharmana.moviecatalogue.util.AppUtils;

import androidx.appcompat.app.AppCompatActivity;

public class MovieDetailActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "extra_movie";

    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        movie = getIntent().getParcelableExtra(EXTRA_MOVIE);

        fetchDetail();

    }

    private void fetchDetail() {
        TextView txtTitle = findViewById(R.id.txt_title);
        TextView txtYear = findViewById(R.id.txt_year);
        TextView txtVote = findViewById(R.id.txt_vote_average);
        TextView txtDescription = findViewById(R.id.txt_description);
        ImageView imgPhoto = findViewById(R.id.img_poster);

        if(!TextUtils.isEmpty(movie.getTitle())) {
            txtTitle.setText(movie.getTitle());
        } else {
            txtTitle.setText(movie.getName());
        }

        txtDescription.setText(movie.getOverview());

        if(movie.getReleaseDate()!=null) {
            txtYear.setText(movie.getReleaseDate().split("-")[0]);
        } else {
            txtYear.setText(movie.getFirstAirDate().split("-")[0]);
        }
        txtVote.setText(movie.getVoteAverage()+"");

        String posterUrl = AppUtils.Companion.getPosterUrl(movie.getPosterPath());

        Glide.with(this)
                .load(posterUrl)
                .thumbnail(0.3F)
                .into(imgPhoto);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_fav) {

        }
        return super.onOptionsItemSelected(item);
    }

}
