package com.wdharmana.moviecatalogue.view.home;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.wdharmana.moviecatalogue.R;
import com.wdharmana.moviecatalogue.model.Movie;
import com.wdharmana.moviecatalogue.util.ItemClickSupport;
import com.wdharmana.moviecatalogue.view.detail.MovieDetailActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemFragment extends Fragment {


    private ArrayList<Movie> moviesList;

    private MovieAdapter adapter;
    private RecyclerView listView;

    private FrameLayout layoutLoader;
    private ImageView imgError;

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    MovieViewModel viewModel;


    public ItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_item, container, false);

        listView = view.findViewById(R.id.lv_list);
        layoutLoader = view.findViewById(R.id.layout_loader);
        imgError = view.findViewById(R.id.img_error);

        return view;
    }

    @Override
    public void onAttach(Context context) {

        AndroidSupportInjection.inject(this);
        super.onAttach(context);


        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieViewModel.class);
        observeViewModel();
    }

    private void observeViewModel() {

        viewModel.isShowLoader().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isShowLoader) {
                if(isShowLoader!=null) {
                    if (isShowLoader) {
                        imgError.setVisibility(View.GONE);
                        layoutLoader.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                    } else {
                        imgError.setVisibility(View.GONE);
                        layoutLoader.setVisibility(View.GONE);
                        listView.setVisibility(View.GONE);
                    }
                }
            }
        });

        viewModel.getErrorMsg().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(s!=null) {
                    imgError.setVisibility(View.VISIBLE);
                    layoutLoader.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                } else {
                    imgError.setVisibility(View.GONE);
                    layoutLoader.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                }
            }
        });

        viewModel.getMovieListLiveData().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {

                if(movies!=null) {
                    listView.setVisibility(View.VISIBLE);
                    moviesList = (ArrayList<Movie>) movies;
                    adapter.setMovies((ArrayList<Movie>) movies);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();

        listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MovieAdapter(getActivity());
        listView.setAdapter(adapter);

        ItemClickSupport.addTo(listView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                showSelectedItem(moviesList.get(position));
            }
        });

        if(bundle!=null) {

            String type = bundle.getString("type");
            if(type !=null) {
                if (type.equals("MOVIE")) {
                    viewModel.getMovies();
                } else {
                    viewModel.getTvShow();
                }
            }

        }

    }

    private void showSelectedItem(Movie movie){
        Intent detailIntent = new Intent(getActivity(), MovieDetailActivity.class);
        detailIntent.putExtra(MovieDetailActivity.EXTRA_MOVIE, movie);
        startActivity(detailIntent);
    }



}
