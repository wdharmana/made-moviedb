package com.wdharmana.moviecatalogue.view.home;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wdharmana.moviecatalogue.R;
import com.wdharmana.moviecatalogue.model.Movie;
import com.wdharmana.moviecatalogue.util.AppUtils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private ArrayList<Movie> movies;

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public MovieAdapter(Context context) {
        Context context1 = context;
        movies = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

        Movie movie = movies.get(i);

        if(!TextUtils.isEmpty(movie.getTitle())) {
            holder.txtTitle.setText(movie.getTitle());
        } else {
            holder.txtTitle.setText(movie.getName());
        }
        holder.txtDescription.setText(movie.getOverview());

        if(movie.getPosterPath()!=null) {
            String posterUrl = AppUtils.Companion.getPosterUrl(movie.getPosterPath());

            Glide.with(holder.imgPhoto.getContext())
                    .load(posterUrl)
                    .thumbnail(0.3F)
                    .into(holder.imgPhoto);
        }

    }


    @Override
    public int getItemCount() {
        return movies.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtTitle;
        private TextView txtDescription;
        private ImageView imgPhoto;

        ViewHolder(View view) {
            super(view);
            txtTitle = view.findViewById(R.id.txt_title);
            txtDescription = view.findViewById(R.id.txt_description);
            imgPhoto = view.findViewById(R.id.img_poster);
        }

    }

}
