package com.wdharmana.moviecatalogue.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "favorites")
class Favorite(
        @PrimaryKey val id: String,
        @ColumnInfo(name = "title") val title: String,
        @ColumnInfo(name = "poster_path") val posterPath: String,
        @ColumnInfo(name = "backdrop_path") val backdropPath: String,
        @ColumnInfo(name = "overview") val overview: String,
        @ColumnInfo(name = "release_date") val releaseDate: String,
        @ColumnInfo(name = "vote_average") val voteAverage: Double,
        @ColumnInfo(name = "category") val category: String
)