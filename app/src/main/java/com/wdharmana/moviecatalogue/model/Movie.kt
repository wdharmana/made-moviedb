package com.wdharmana.moviecatalogue.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Movie(

        @SerializedName("overview")
        var overview: String? = null,

        @SerializedName("original_language")
        var originalLanguage: String? = null,

        @SerializedName("original_title")
        var originalTitle: String? = null,

        @SerializedName("video")
        var video: Boolean? = null,

        @SerializedName("title")
        var title: String? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("genre_ids")
        var genreIds: List<Int> = listOf(),

        @SerializedName("poster_path")
        var posterPath: String? = null,

        @SerializedName("backdrop_path")
        var backdropPath: String? = null,

        @SerializedName("release_date")
        var releaseDate: String? = null,

        @SerializedName("first_air_date")
        var firstAirDate: String? = null,

        @SerializedName("vote_average")
        var voteAverage: Double = 0.0,

        @SerializedName("popularity")
        var popularity: Double? = null,

        @SerializedName("id")
        var id: String? = null,

        @SerializedName("vote_count")
        var voteCount: Int = 0,

        @SerializedName("category")
        var category: String? = "movie"
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readString(),
            source.readString(),
            ArrayList<Int>().apply { source.readList(this, Int::class.java.classLoader) },
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Double::class.java.classLoader) as Double,
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readString(),
            source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(overview)
        writeString(originalLanguage)
        writeString(originalTitle)
        writeValue(video)
        writeString(title)
        writeString(name)
        writeList(genreIds)
        writeString(posterPath)
        writeString(backdropPath)
        writeString(releaseDate)
        writeString(firstAirDate)
        writeValue(voteAverage)
        writeValue(popularity)
        writeValue(id)
        writeValue(voteCount)
        writeValue(category)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Movie> = object : Parcelable.Creator<Movie> {
            override fun createFromParcel(source: Parcel): Movie = Movie(source)
            override fun newArray(size: Int): Array<Movie?> = arrayOfNulls(size)
        }
    }
}