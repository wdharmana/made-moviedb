package com.wdharmana.moviecatalogue.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wdharmana.moviecatalogue.model.Favorite
import io.reactivex.Single

@Dao
interface FavoriteDao {

    @Query("SELECT * FROM favorites WHERE category=:cat")
    fun getFavoritesByCat(cat: String): Single<List<Favorite>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavorite(favorite: Favorite)

    @Query("DELETE FROM favorites WHERE id=:id")
    fun deleteFavorite(id: String)

}