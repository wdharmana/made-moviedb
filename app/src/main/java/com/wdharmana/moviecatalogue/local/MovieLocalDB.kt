package com.wdharmana.moviecatalogue.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wdharmana.moviecatalogue.model.Favorite

@Database(version = 1,entities = [Favorite::class],exportSchema = false)
abstract class MovieLocalDB : RoomDatabase() {
    abstract fun favoriteDao(): FavoriteDao
}