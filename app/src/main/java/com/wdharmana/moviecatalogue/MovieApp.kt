package com.wdharmana.moviecatalogue

import android.app.Activity
import android.app.Application
import com.wdharmana.moviecatalogue.di.component.DaggerAppComponent
import com.wdharmana.moviecatalogue.di.module.AppModule
import com.wdharmana.moviecatalogue.di.module.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class MovieApp : Application(), HasActivityInjector {


    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>


    init {
        instance = this
    }
    companion object {
        private var instance: MovieApp? = null
    }

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule())
                .build().inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

}