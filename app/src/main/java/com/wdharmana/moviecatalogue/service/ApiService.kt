package com.wdharmana.moviecatalogue.service

import com.wdharmana.moviecatalogue.BuildConfig
import com.wdharmana.moviecatalogue.model.MovieResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("discover/movie?api_key="+BuildConfig.API_KEY)
    fun getMovies(): Single<Response<MovieResponse>>

    @GET("discover/tv?api_key="+BuildConfig.API_KEY)
    fun getTvShow(): Single<Response<MovieResponse>>

}