package com.wdharmana.moviecatalogue.service

import com.wdharmana.moviecatalogue.local.FavoriteDao
import com.wdharmana.moviecatalogue.model.Favorite
import com.wdharmana.moviecatalogue.model.MovieResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject
constructor(private val api: ApiService, private val favoriteDao: FavoriteDao) {

    fun getMovies(): Single<Response<MovieResponse>> {
        return api.getMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getTvShow(): Single<Response<MovieResponse>> {
        return api.getTvShow()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getFavorites(cat: String): Single<List<Favorite>> {
        return favoriteDao.getFavoritesByCat(cat)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /*
    fun insertFavorite(favorite: Favorite): Single<Int> {
        return favoriteDao.insertFavorite(favorite)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun deleteFavorite(id: String): Single<Int> {
        return favoriteDao.deleteFavorite(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
    */

}